# Namespaces
PRODUCT_SOONG_NAMESPACES += \
    vendor/GoogleCamera

# Packages
PRODUCT_PACKAGES += \
    GoogleCamera
